#!/bin/bash
## Yume wo kanaete on PC Speaker (2005 Doraemon OST)
## You're allowed to redistribute, modify, share without my permissions.

## On Ubuntu, PC speaker module is disabled by default, you need to enable it.

## Intro
beep -f 174.614 -l 210 -D 210
beep -f 130.813 -l 50 -D 50
beep -f 174.614 -l 210 -D 210
beep -f 130.813 -l 50 -D 50
beep -f 174.614 -l 210 -D 210
beep -f 130.813 -l 50 -D 50
beep -f 174.614 -l 210 -D 210
beep -f 130.813 -l 50 -D 50
beep -f 174.614 -l 210 -D 210
beep -f 130.813 -l 50 -D 50
beep -f 174.614 -l 210 -D 210
beep -f 130.813 -l 50 -D 50
beep -f 174.614 -l 210 -D 210
beep -f 130.813 -l 50 -D 50
beep -f 174.614 -l 210 -D 210
beep -f 130.813 -l 50 -D 50
beep -f 174.614 -l 210 -D 210
beep -f 130.813 -l 50 -D 50
beep -f 174.614 -l 210 -D 210
beep -f 130.813 -l 50 -D 50
beep -f 174.614 -l 210 -D 210
beep -f 130.813 -l 50 -D 50
beep -f 130.813 -l 50 -D 100
beep -f 130.813 -l 50 -D 100
beep -f 174.614 -l 100 -D 100
beep -f 207.652 -l 150 -D 100
beep -f 261.626 -l 50 -D 50
beep -f 174.614 -l 100 -D 100
beep -f 207.652 -l 150 -D 100
beep -f 233.082 -l 50 -D 50
beep -f 146.832 -l 50 -D 50
beep -f 233.082 -l 120 -D 120
beep -f 220.000 -l 50 -D 50
beep -f 233.082 -l 50 -D 50
beep -f 130.813 -l 100 -D 100
beep -f 220.000 -l 100 -D 100

## Mainstream song!! (koko na ega it sumo itsumo)
beep -f 130.813 -l 170 -D 170
beep -f 130.813 -l 100 -D 100
beep -f 146.832 -l 300 -D 100
beep -f 146.832 -l 100 -D 100
beep -f 174.614 -l 300 -D 100
beep -f 195.998 -l 150 -D 345

## Repeat above
beep -f 130.813 -l 170 -D 170
beep -f 130.813 -l 100 -D 100
beep -f 146.832 -l 300 -D 100
beep -f 146.832 -l 100 -D 100
beep -f 174.614 -l 300 -D 100
beep -f 195.998 -l 150 -D 310

## Egaiteru

beep -f 220.000 -l 200 -D 10
beep -f 220.000 -l 270 -D 35
beep -f 195.998 -l 270 -D 20
beep -f 184.997 -l 250 -D 50
beep -f 195.998 -l 700 -D 100

beep -f 220.000 -l 200 -D 10
beep -f 220.000 -l 270 -D 35
beep -f 195.998 -l 270 -D 20
beep -f 184.997 -l 250 -D 50
beep -f 195.998 -l 700 -D 100

## Repeat once again
beep -f 130.813 -l 170 -D 170
beep -f 130.813 -l 100 -D 100
beep -f 146.832 -l 300 -D 100
beep -f 146.832 -l 100 -D 100
beep -f 174.614 -l 300 -D 100
beep -f 195.998 -l 150 -D 345
beep -f 130.813 -l 170 -D 170
beep -f 130.813 -l 100 -D 100
beep -f 146.832 -l 300 -D 100
beep -f 146.832 -l 100 -D 100
beep -f 174.614 -l 300 -D 100
beep -f 195.998 -l 150 -D 310
beep -f 233.082 -l 200 -D 10
beep -f 233.082 -l 270 -D 35
beep -f 195.998 -l 270 -D 20
beep -f 184.997 -l 250 -D 50
beep -f 233.082 -l 700 -D 100

## Next part of that last one

beep -f 369.994 -l 270 -D 10
beep -f 329.628 -l 320 -D 35
beep -f 293.665 -l 260 -D 20
beep -f 261.626 -l 240 -D 50
beep -f 233.082 -l 100 -D 50
beep -f 220.000 -l 200 -D 50
beep -f 233.082 -l 150 -D 50
beep -f 220.000 -l 200 -D 50
beep -f 195.998 -l 100 -D 50

beep -f 130.813 -l 170 -D 170
beep -f 130.813 -l 100 -D 100
beep -f 146.832 -l 300 -D 100
beep -f 146.832 -l 100 -D 100
beep -f 174.614 -l 300 -D 100
beep -f 195.998 -l 150 -D 345
beep -f 130.813 -l 170 -D 170
beep -f 130.813 -l 100 -D 100
beep -f 146.832 -l 300 -D 100
beep -f 146.832 -l 100 -D 100
beep -f 174.614 -l 300 -D 100
beep -f 195.998 -l 150 -D 310

beep -f 220.000 -l 200 -D 10
beep -f 220.000 -l 270 -D 35
beep -f 174.614 -l 270 -D 20
beep -f 184.997 -l 250 -D 50
beep -f 195.998 -l 400 -D 100
beep -f 146.832 -l 300 -D 100
beep -f 195.998 -l 1000 -D 100

## Open the door and looks w t f is happening over there!?!?

beep -f 146.832 -l 300 -D 50
beep -f 164.814 -l 250 -D 50
beep -f 146.832 -l 170 -D 50
beep -f 130.813 -l 170 -D 50
beep -f 130.813 -l 270 -D 50
beep -f 146.832 -l 210 -D 50
beep -f 174.614 -l 340 -D 50
beep -f 174.614 -l 100 -D 300
beep -f 195.998 -l 470 -D 50
beep -f 220.000 -l 200 -D 50
beep -f 233.082 -l 500 -D 50
beep -f 220.000 -l 200 -D 50
beep -f 195.998 -l 170 -D 100
beep -f 195.998 -l 470 -D 50
beep -f 174.614 -l 110 -D 150
beep -f 174.614 -l 1300 -D 100

## When you grow up

beep -f 349.228 -l 350 -D 100
beep -f 329.628 -l 350 -D 100
beep -f 311.127 -l 350 -D 100
beep -f 293.665 -l 350 -D 100
beep -f 349.228 -l 350 -D 100
beep -f 329.628 -l 350 -D 100
beep -f 391.995 -l 350 -D 100
beep -f 349.228 -l 350 -D 100
beep -f 261.626 -l 650 -D 100
beep -f 220.000 -l 100 -D 60
beep -f 223.082 -l 350 -D 60
beep -f 293.665 -l 350 -D 60
beep -f 261.626 -l 350 -D 60
beep -f 223.082 -l 150 -D 200
beep -f 261.626 -l 700 -D 150
beep -f 293.665 -l 350 -D 60
beep -f 349.228 -l 350 -D 60
beep -f 329.628 -l 350 -D 60
beep -f 293.665 -l 350 -D 60
beep -f 261.626 -l 350 -D 60
beep -f 391.995 -l 300 -D 60
beep -f 349.228 -l 300 -D 500
beep -f 349.228 -l 300 -D 60
beep -f 391.995 -l 300 -D 60
beep -f 391.995 -l 300 -D 60
beep -f 349.228 -l 300 -D 60
beep -f 329.628 -l 300 -D 60
beep -f 293.665 -l 300 -D 60
beep -f 329.628 -l 300 -D 60
beep -f 349.228 -l 300 -D 60
beep -f 369.944 -l 300 -D 60
beep -f 391.995 -l 300 -D 200

## sudo la la la la ;)
beep -f 440.000 -l 200 -D 60
beep -f 261.626 -l 200 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 261.626 -l 200 -D 60
beep -f 440.000 -l 200 -D 60

## Lucks will come with you
beep -f 261.626 -l 200 -D 60
beep -f 349.228 -l 200 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 466.164 -l 300 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 391.995 -l 400 -D 250

## For the love, everythings comes
beep -f 329.628 -l 200 -D 60
beep -f 233.082 -l 200 -D 60
beep -f 329.628 -l 200 -D 60
beep -f 233.082 -l 200 -D 60
beep -f 329.628 -l 200 -D 60

beep -f 261.626 -l 200 -D 60
beep -f 329.628 -l 200 -D 60
beep -f 391.995 -l 200 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 466.164 -l 300 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 440.000 -l 400 -D 60

## Doraemon, with every black magics, you can show with
beep -f 440.000 -l 170 -D 60
beep -f 261.626 -l 170 -D 60
beep -f 440.000 -l 170 -D 60
beep -f 261.626 -l 170 -D 60
beep -f 440.000 -l 170 -D 60

beep -f 369.994 -l 300 -D 60
beep -f 466.164 -l 200 -D 60
beep -f 523.251 -l 300 -D 60
beep -f 523.251 -l 200 -D 60
beep -f 466.164 -l 200 -D 60
beep -f 466.164 -l 200 -D 60
beep -f 440.000 -l 300 -D 60
beep -f 466.164 -l 300 -D 60
beep -f 440.000 -l 300 -D 60

beep -f 466.164 -l 300 -D 60
beep -f 440.000 -l 300 -D 60
beep -f 391.995 -l 300 -D 60
beep -f 349.228 -l 300 -D 60
beep -f 329.628 -l 400 -D 60
beep -f 349.228 -l 400 -D 60
beep -f 349.228 -l 600 -D 60

## Some pianos again
beep -f 349.228 -l 100 -D 60
beep -f 329.628 -l 100 -D 60
beep -f 293.665 -l 100 -D 60
beep -f 261.626 -l 100 -D 60
beep -f 293.665 -l 100 -D 60
beep -f 329.628 -l 130 -D 60
beep -f 329.628 -l 160 -D 60
beep -f 349.228 -l 100 -D 60
beep -f 391.995 -l 160 -D 60

## Sha la la la la again lol
beep -f 440.000 -l 200 -D 60
beep -f 261.626 -l 200 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 261.626 -l 200 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 261.626 -l 200 -D 300

beep -f 261.626 -l 200 -D 60
beep -f 349.228 -l 200 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 466.164 -l 300 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 391.995 -l 400 -D 60


beep -f 329.628 -l 200 -D 60
beep -f 233.082 -l 200 -D 60
beep -f 329.628 -l 200 -D 60
beep -f 233.082 -l 200 -D 60
beep -f 329.628 -l 200 -D 60

beep -f 261.626 -l 200 -D 60
beep -f 329.628 -l 200 -D 60
beep -f 391.995 -l 200 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 466.164 -l 300 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 440.000 -l 400 -D 60

## Doraemon, we fly to the linux tower, for the world, grows and beat out of windows. kek
beep -f 440.000 -l 200 -D 60
beep -f 261.626 -l 200 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 261.626 -l 200 -D 60
beep -f 440.000 -l 200 -D 60
beep -f 369.994 -l 300 -D 60
beep -f 466.164 -l 200 -D 60
beep -f 523.251 -l 300 -D 60
beep -f 523.251 -l 200 -D 60
beep -f 466.164 -l 200 -D 60
beep -f 466.164 -l 200 -D 60
beep -f 440.000 -l 300 -D 60
beep -f 466.164 -l 300 -D 60
beep -f 440.000 -l 300 -D 60
beep -f 466.164 -l 300 -D 60
beep -f 440.000 -l 300 -D 500
beep -f 391.995 -l 300 -D 500
beep -f 349.228 -l 300 -D 60
beep -f 329.628 -l 400 -D 60
beep -f 349.228 -l 400 -D 60
beep -f 349.228 -l 700 -D 60

## End
beep -f 700 -l 300